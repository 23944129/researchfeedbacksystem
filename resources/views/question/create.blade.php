@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h1>Create New Question</h1>
                </div>

                <div class="card-body">
                    <form action="/questionnaires/{{ $questionnaire->id }}/questions" method="post" id="createQuestion">
                     @csrf
                        <div class="form-group">
                            <label for="question">Question</label>
                            <input name="question[question]" type="text" class="form-control" id="question"
                                aria-describedby="questionHint" value="{{ old('question.question') }}" placeholder="Enter a Question"></input>
                            <small id="questionHint" class="form-text text-muted">Enter your choice of question.</small>
                            <small class="text-danger">{{$errors->first('question.question')}}</small>
                        </div>

                        <div class="form-group">
                            <fieldset>
                                <legend>Choices</legend>
                                <small id="choicesHint" class="form-text text-muted">Enter your choices.</small>

                                <div class="form-group">
                                    <label for="answer1">Choice 01</label>
                                    <input name="answers[][answer] "type="text" class="form-control" id="answer1"
                                        aria-describedby="choicesHelp" placeholder="Choice 01">
                                    <small class="text-danger">{{$errors->first('answers.0.answer')}}</small>
                                </div>
                                <div class="form-group">
                                    <label for="answer2">Choice 02</label>
                                    <input name="answers[][answer] "type="text" class="form-control" id="answer2"
                                        aria-describedby="choicesHelp" placeholder="Choice 02">
                                    <small class="text-danger">{{$errors->first('answers.1.answer')}}</small>
                                </div>
                                <div class="form-group">
                                    <label for="answer3">Choice 03</label>
                                    <input name="answers[][answer] "type="text" class="form-control" id="answer3"
                                        aria-describedby="choicesHelp" placeholder="Choice 03">
                                    <small class="text-danger">{{$errors->first('answers.2.answer')}}</small>
                                </div>
                                <div class="form-group">
                                    <label for="answer4">Choice 04</label>
                                    <input name="answers[][answer] "type="text" class="form-control" id="answer4"
                                        aria-describedby="choicesHelp" placeholder="Choice 04">
                                    <small class="text-danger">{{$errors->first('answers.3.answer')}}</small>
                                </div>
                                <div class="form-group">
                                    <label for="answer5">Choice 05</label>
                                    <input name="answers[][answer] "type="text" class="form-control" id="answer5"
                                        aria-describedby="choicesHelp" placeholder="Choice 05">
                                    <small class="text-danger">{{$errors->first('answers.4.answer')}}</small>
                                </div>
                            </fieldset>
                        </div>
                        <button type="submit" class="btn btn-success">Save Question</button>
                    </form>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
