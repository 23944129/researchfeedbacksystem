@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>{{ $questionnaire->title }}</h1>
                </div>

                <div class="card-body">
                    <p>{{ $questionnaire->description }}</p>

                    <a class="btn btn-info" href="/questionnaires/{{ $questionnaire->id}}/questions/create">Add Question</a>
                    <a class="btn btn-dark" href="/surveys/{{ $questionnaire->id}}-{{ Str::slug($questionnaire->title) }}">Take Survey</a>
                </div>
            </div>

            @foreach($questionnaire->questions as $question)

                <div class="card">
                    <div class="card-header">
                        <h3>{{ $question->question }}</h3>
                    </div>

                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($question->answers as $answer)
                                <li class="list-group-item">{{ $answer->answer }}</li>
                            @endforeach
                        </ul>
                        <a class="btn btn-info" href="/questionnaires/{{ $questionnaire->id}}/questions/create">Add Question</a>
                    </div>
                </div>

            @endforeach

        </div>
    </div>
</div>
@endsection
