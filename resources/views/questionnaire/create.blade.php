@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h1>Create New Questionnaire</h1>
                </div>

                <div class="card-body">
                    <form action="/questionnaires" method="post" id="createQuestionnaire">
                     @csrf
                        <div class="form-group">
                            <label for="title">Questionnaire Title</label>
                            <input name="title" type="text" class="form-control" id="title" aria-describedby="titleHint" placeholder="Enter Questionnaire Title">
                            <small id="titleHint" class="form-text text-muted">Enter a relevant title.</small>
                            <small class="text-danger">{{$errors->first('title')}}</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Questionnaire Description</label>
                            <input name="description" type="text" class="form-control" id="description" aria-describedby="descriptionHint" placeholder="Enter Questionnaire Description">
                            <small id="descriptionHint" class="form-text text-muted">Enter an appropriate description.</small>
                            <small class="text-danger">{{$errors->first('description')}}</small>
                        </div>
                        <button type="submit" class="btn btn-info">Add Questionnaire</button>
                    </form>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
