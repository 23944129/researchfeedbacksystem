@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
                
                    <h1 class="text-center">{{ $questionnaire->title }}</h1>
            

                <form action="#" method="post">
                @csrf

                @foreach($questionnaire->questions as $key => $question)

                <div class="card">
                    <div class="card-header">
                        <h5>{{ $key + 1}} {{ $question->question }}</h5>
                    </div>

                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($question->answers as $answer)
                                <label for="answer{{ $answer->id }}">
                                    <li class="list-group-item">
                                        <input type="radio" name="responses[{{ $key }}][answer_id]" id="answer{{ $answer->id }}">
                                        {{ $answer->answer }}
                                    </li>
                                </label>
                            @endforeach
                        </ul>
                        <a class="btn btn-info" href="/questionnaires/{{ $questionnaire->id}}/questions/create">Add Question</a>
                    </div>
                </div>

            @endforeach
                
                </form>



            </div>
        </div>
    </div>
</div>
@endsection
