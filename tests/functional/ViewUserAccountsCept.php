<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('view user accounts');

//User records to display

$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

$I->haveRecord('users',[
    'id' => '777',
    'name' => 'testuser2',
    'email' => 'email@fake.com',
    'password' => 'password1',
]);

$I->seeRecord('users', ['id' => '666', 'name' => 'testuser']);
//And
$I->seeRecord('users', ['id' => '777', 'name' => 'testuser2']);


//When
$I->amOnPage('/admin/users');

//And
$I->see('Users', 'h1');
$I->see('testuser');
$I->see('testuser2');
