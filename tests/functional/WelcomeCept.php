<?php 
$I = new FunctionalTester($scenario);

$I->am('an admin');
$I->wantTo('test Laravel is working');

//When
$I->amOnPage('/');

//Then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel', '.title');
