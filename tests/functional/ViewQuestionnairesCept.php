<?php 
$I = new FunctionalTester($scenario);

$I->am('respondent');
$I->wantTo('view questionnaires');

//Questionnaire records to display
$I->haveRecord('questionnaires',[
    'id' => '6006',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'slug' => 'questionnaire1',
    'user_id' => '666', 
]);

$I->haveRecord('questionnaires',[
    'id' => '7000',
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
    'slug' => 'questionnaire2',
    'user_id' => '666', 
]);

$I->seeRecord('questionnaires', ['id' => '6006', 'title' => 'Questionnaire 1']);
//And
$I->seeRecord('questionnaires', ['id' => '7000', 'name' => 'Questionnaire 2']);


//When
$I->amOnPage('/questionnaires');

//And
$I->see('Questionnaires', 'h1');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');
