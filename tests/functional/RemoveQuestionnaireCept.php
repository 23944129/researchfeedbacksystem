<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('remove a questionnaire');

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is being listed
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'slug' => 'questionnaire1',
    'user_id' => '666', 
]);


$I->seeRecord('questionnaires',[
    'id' => '6000',
    'title' => 'Questionnaire 1',
]);


//When
$I->amOnPage('/researcher/questionnaires');
$I->see('Questionnaires', 'h1');
$I->seeElement('a','Questionnaire 1');

//And
$I->click('Delete Questionnaire 1');

//Then
$I->amOnPage('/researcher/questionnaires');

//And
$I->see('Questionnaires', 'h1');
$I->dontSeeElement('a','Questionnaire 1');