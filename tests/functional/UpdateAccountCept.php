<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('update account details');

//DB test user to update
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

$I->seeRecord('users', ['name' => 'testuser', 'id' => '666']);

//When
$I->amOnPage('/admin/users');

//And
$I->seeElement('a', ['name' => 'testuser']);

//Then
$I->click('a', ['name' => 'testuser']);

//Then
$I->amOnPage('/admin/users/666/edit');

//And
$I->see('Edit User - testuser', 'h1');

$I->fillField('name', 'updatedname');

//And
$I->click('Update User');

//Then
$I->amOnPage('/admin/users');
$I->seeRecord('users', ['name'=> 'updatedname']);
$I->see('Users', 'h1');
$I->see('updatedname');