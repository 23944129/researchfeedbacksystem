<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('update a questionnaires details');

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is being listed
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'active' => '0',
    'user_id' => '666', 
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'ethic_agree' => '0',
]);
 

//When
$I->amOnPage('/researcher/questionnaires');
$I->see('Questionnaires', 'h1');

//Then
$I->seeElement('a', ['title' => 'Questionnaire 1']);

//And
$I->click('a', ['title' => 'Questionnaire 1']);

//Then
$I->amOnPage('/researcher/questionnaires/1');

//And
$I->click('a', ['Edit']);

//Then
$I->amOnPage('/researcher/questionnaires/1/edit');

//And
$I->see('Edit Questionnaire - Questionnaire 1', 'h1');

//Then
$I->fillField('title', 'Updatedtitle');

//And 
$I->click('Update Article');

//Then
$I->amOnPage('/researcher/questionnaires');
$I->seeRecord('questionnaires', ['title' => 'Updatedtitle']);
$I->see('Questionnaires', 'h1');
$I->see('Updatedtitle');
