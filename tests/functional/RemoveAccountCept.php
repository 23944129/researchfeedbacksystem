<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a user account');

//Add test user without modifying db
$I->haveRecord('users', [
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com'
]);

//Check user in db
$I->seeRecord('users', ['id' => '666', 'name' => 'testuser']);

//When
$I->amOnPage('/admin/users');

//Then
$I->see('testuser');
$I->seeElement('testuser', 'a.item');

//And 
$I->click('testuser delete');

//Then
$I->amOnPage('/admin/users');

//And
$I->dontSee('testuser');
$I->dontSeeElement('testuser', 'a.item');