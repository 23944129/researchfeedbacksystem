<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('update question details');

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is being listed

$I->haveRecord('questionnaires',[
    'questionnaireID' => '6000',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'slug' => 'questionnaire1',
    'user_id' => '666', 
]);
 
$I->haveRecord('questions', [
    'questionID' => '1',
    'questionnaireID' => '6000',
    'questionContent' => 'Question 1',
    'answer1' => 'Answer 1',
    'answer2' => 'Answer 2',
    'answer3' => 'Answer 3',
    'answer4' => 'Answer 4',
    'answer5' => 'Answer 5',
]);


//When
$I->amOnPage('/researcher/questionnaires');
$I->see('Questionnaires', 'h1');

//Then
$I->seeElement('a', ['Questionnaire 1']);

//And
$I->click('a', ['Questionnaire 1']);

//Then
$I->amOnPage('/researcher/questionnaires/1');

//And
$I->see('Questionnaire 1', 'h1');
$I->see('Question 1');

//Then 
$I->click('a', 'edit');

//Then
$I->amOnPage('/researcher/questionnaires/1/question/edit');

//And
$I->fillField('questionContent', 'Updatedcontent');

//And 
$I->click('Update Question');

//Then
$I->amOnPage('/researcher/questionnaires/1');
$I->seeRecord('questions', ['questionContent' => 'Updatedcontent']);
$I->see('Questionnaire 1', 'h1');
$I->see('Updatedcontent');
