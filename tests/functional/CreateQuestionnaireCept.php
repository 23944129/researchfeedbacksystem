<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new questionnaire');

Auth::loginUsingId(1);

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is being added to db
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'active' => '0',
    'user_id' => '666', 
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'ethic_agree' => '0',
]);
 
//When
$I->amOnPage('home');
$I->see('My Questionnaires');

//And
$I->click('Create Questionnaire');

//Then
$I->amOnPage('/questionnaires/create');

//And
$I->see('Create New Questionnaire', 'h1');

$I->submitForm('#createQuestionnaire', [
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
]);

//When
$I->seeCurrentUrlMatches('~/questionnaires/(\d+)~');
$I->see('Questionnaire 2', 'h1');


// $I->see('Questionnaire 2 description');
// // $I->see('creator: testuser');

// //Check for duplication
// //When
// $I->amOnPage('/questionnaires');
// $I->see('Questionnaires','h1');
// // $I->see('New questionnaire added!');
// $I->see('Questionnaire 2');

// //And
// $I->click('Create Questionnaire');

// //Then
// $I->amOnPage('/researchers/questionnaires/create');

// //And
// $I->see('Create New Questionnaire', 'h1');

// $I->submitForm('#createQuestionnaire', [
//     'title' => 'Questionnaire 2',
//     'description' => 'Questionnaire 2 description',
//     'slug' => 'questionnaire2', 
// ]);

// //Then
// $I->amOnPage('/researchers/questionnaires');
// $I->see('Questionnaires', 'h1');
// // $I->see('Error - Questionnaire already exists with that title!');