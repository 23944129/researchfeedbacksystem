<?php
//Functional test for adding a new questionnaire to a researchers account and ensuring they don't use duplicate titles.
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new question');


Auth::loginUsingId(1);

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is added to db
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'active' => '0',
    'user_id' => '666',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'ethic_agree' => '0',
     
]);

//Add test question to show content being added to db
$I->haveRecord('questions',[
    'id' => '6002',
    'questionnaire_id' => '6000',
    'question' => 'How are you?'
]);


//When
$I->amOnPage('home');
$I->see('My Questionnaires');

//And
$I->click('Create Questionnaire');

//Then
$I->amOnPage('/questionnaires/create');

$I->see('Create New Questionnaire', 'h1');

$I->submitForm('#createQuestionnaire', [
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
]);

 $I->seeRecord('questionnaires', ['title' => 'Questionnaire 2']);
//When
$I->seeCurrentUrlMatches('~/questionnaires/(\d+)~');
// $I->amOnPage('/questionnaires/2');
$I->see('Questionnaire 2', 'h1');

//And
$I->click('Add Question');

//Then
$I->seeCurrentUrlMatches('~/questionnaires/(\d+)/questions/create~');

//And
$I->see('Create New Question', 'h1');

//Then
$I->submitForm('#createQuestion', [
    'question' => 'Question 1',
    'answer1' => 'Answer 1',
    'answer2' => 'Answer 2',
    'answer3' => 'Answer 3',
    'answer4' => 'Answer 4',
    'answer5' => 'Answer 5', 
]);

 $I->seeRecord('questions', ['question' => 'Question 1']);

// $I->click('Save Question');
//Then
// $I->amOnPage('/questionnaires/2');

$I->seeCurrentUrlMatches('~/questionnaires/(\d+)~');
;
$I->see('Questionnaire 2', 'h1');
$I->see('Question 1');
