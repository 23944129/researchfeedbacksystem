<?php 
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('log in to account');

//Add test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password'
]);

$I->seeRecord('users', ['email' => 'fake@email.com', 'password' => 'password']);

//When
$I->amOnPage('/login');

//And
$I->see('Login');

//And
$I->dontSee('You are logged in!');

//Then
$I->click('Login');

//And 
$I->amOnPage('/login');

$I->fillField('email', 'fake@email.com');
$I->fillField('password', 'password');

// $I->submitForm('#login', [
//     'email' => 'fake@email.com',
//     'password' => 'password',
// ]);
$I->click('Login');
//Then
$I->amOnPage('/home');

//And
$I->see('Dashboard');