<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('view a questionnaire');


Auth::loginUsingId(1);

//Add db test user
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);


//Questionnaire records to display
$I->haveRecord('questionnaires',[
    'id' => '6006',
    'active' => '0',
    'user_id' => '666',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'ethic_agree' => '0', 
]);

$I->haveRecord('questionnaires',[
    'id' => '6007',
    'active' => '0',
    'user_id' => '666',
    'title' => 'Questionnaire 2',
    'description' => 'Questionnaire 2 description',
    'ethic_agree' => '0', 
]);


$I->seeRecord('questionnaires', ['id' => '6006', 'title' => 'Questionnaire 1']);
$I->seeRecord('questionnaires', ['id' => '6007', 'title' => 'Questionnaire 2']);

//And

//When
$I->amOnPage('home');

//And
$I->see('My Questionnaires');
$I->see('Questionnaire 1');
$I->see('Questionnaire 2');

//And
$I->click('Questionnaire 1');

//Then
$I->seeCurrentUrlMatches('~/questionnaires/(\d+)~');
$I->see('Questionnaire 1', 'h1');
