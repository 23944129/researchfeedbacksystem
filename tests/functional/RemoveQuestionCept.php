<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('remove a question');

//Add testuser
$I->haveRecord('users',[
    'id' => '666',
    'name' => 'testuser',
    'email' => 'fake@email.com',
    'password' => 'password',
]);

//Add test questionnaire to show that content is being listed
$I->haveRecord('questionnaires',[
    'id' => '6000',
    'title' => 'Questionnaire 01',
    'description' => 'Questionnaire 01 description',
    'slug' => 'questionnaire01',
    'user_id' => '666', 
]);

$I->haveRecord('questions', [
    'id' => '1',
    'questionnaire_id' => '6000',
    'content' => 'Question 1',
    'answer1' => 'Answer 1',
    'answer2' => 'Answer 2',
    'answer3' => 'Answer 3',
    'answer4' => 'Answer 4',
    'answer5' => 'Answer 5',
]);

//Link data for questions and questionnaire
$I->haveRecord('question_questionnaire', [
    'question_id' => '1',
    'questionnaire_id' => '6000',
]);

$I->seeRecord('questions',['questionnaire_id' => '6000', 'id'=> '1']);

//When
$I->amOnPage('/researcher/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Questionnaire 1');

//Then
$I->click('Questionnaire 1');

//And
$I->amOnPage('/researcher/questionnaires/1');
$I->see('Questionnaire 1', 'h1');

//And
$I->SeeElement('Question 1', ['content' => 'Question 1']);

//Then
$I->click('Delete Question');

//And
$I->see('Question deleted!');

//Then
$I->amOnPage('/researcher/questionnaires/1');
$I->dontSeeElement('Question 1', ['content' => 'Question 1']);
