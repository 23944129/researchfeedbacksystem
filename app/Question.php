<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question'
    ];

    public function questionnaire()
    {
        return $this->belongsTo(\App\Questionnaire::class);
    }

    public function answers()
    {
        return $this->hasMany(\App\Answer::class);
    }
}
