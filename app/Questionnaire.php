<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
        'title',
        'description',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


     public function questions()
    {
        return $this->hasMany(\App\Question::class);
    }
}



